from django.db import models


class PredictionOutput(models.Model):

    testname = models.CharField(max_length=100)
    testtime = models.DateTimeField(auto_now_add=True)
    payloadmass = models.IntegerField()
    flights = models.IntegerField()
    gridfins = models.BooleanField()
    reused = models.BooleanField()
    legs = models.IntegerField()
    block = models.IntegerField()
    reusedcount = models.IntegerField()
    orbit = models.CharField(max_length=100)
    launchsite = models.CharField(max_length=100)
    outputpredictions = models.CharField(max_length=100)


# , default='NA', editable=False
# Create your models here.
# orbit_geo
#     orbit_gto
#     orbit_heo
#     orbit_iss
#     orbit_leo
#     orbit_meo
#     orbit_po
#     orbit_so
#     orbit_sso
#     orbit_vleo
"""
{
    "testname" : "Test_002",
    "payloadmass" : 525,
    "flights" : 1,
    "gridfins" : "False",
    "reused" : "False",
    "legs" : 0,
    "block" : 1,
    "reusedcount" : 0,
    "orbit" : "orbit_geo",
    "launchsite" : "launchsite_ccsfs_slc_40",
    "outputpredictions": "default"
}
{
    "testname" : "Test_003",
    "payloadmass" : 3170,
    "flights" : 1,
    "gridfins" : "False",
    "reused" : "False",
    "legs" : 0,
    "block" : 1,
    "reusedcount" : 0,
    "orbit" : "orbit_heo",
    "launchsite" : "launchsite_ccsfs_slc_40",
    "outputpredictions": "default"
}
{
    "testname" : "Test_004",
    "payloadmass" : 500,
    "flights" : 1,
    "gridfins" : "False",
    "reused" : "False",
    "legs" : 0,
    "block" : 1,
    "reusedcount" : 0,
    "orbit" : "orbit_so",
    "launchsite" : "launchsite_vafb_slc_4e",
    "outputpredictions": "default"
}
"""

#     launchsite_ccsfs_slc_40
#     launchsite_ksc_lc_39a
#     launchsite_vafb_slc_4e