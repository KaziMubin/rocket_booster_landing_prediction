from django.urls import path
from .views import default_view, data_input

urlpatterns = [
    path('', default_view),
    path('predict/', data_input),
]