from rest_framework import serializers
from .models import PredictionOutput


class PredictionOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = PredictionOutput
        fields = '__all__'