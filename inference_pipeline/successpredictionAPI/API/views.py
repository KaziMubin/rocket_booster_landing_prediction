import joblib
import numpy as np
import pandas
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .models import PredictionOutput
from .serializers import PredictionOutputSerializer
import hopsworks


@api_view(['GET'])
def default_view(request):
    database_data = PredictionOutput.objects.all()
    serialized_data = PredictionOutputSerializer(database_data, many=True)
    return Response(serialized_data.data)


@api_view(['POST'])
def data_input(request):
    serialized_input_data = PredictionOutputSerializer(data=request.data)

    if serialized_input_data.is_valid():
        prediction = get_prediction(serialized_input_data.validated_data)

        if prediction > 0.7:
            temp_dict = {}
            temp_dict.update(serialized_input_data.validated_data)
            temp_dict.update({'outputpredictions': 'Success'})
        else:
            temp_dict = {}
            temp_dict.update(serialized_input_data.validated_data)
            temp_dict.update({'outputpredictions': 'Failure'})
        updated_serialized_input_data = PredictionOutputSerializer(data=temp_dict)

        if updated_serialized_input_data.is_valid():
            updated_serialized_input_data.save()
            return Response(updated_serialized_input_data.data, status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    else:
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


def get_prediction(input_data):

    project = hopsworks.login()
    mr = project.get_model_registry()

    retrieved_model = mr.get_model(name='treemodel', version=1)
    retrieved_model = retrieved_model.download()

    model = joblib.load(retrieved_model + '\\tree.pkl')

    headers = ['payloadmass', 'flights', 'gridfins', 'reused', 'legs', 'block', 'reusedcount', 'orbit_es_l1','orbit_geo',
               'orbit_gto', 'orbit_heo', 'orbit_iss', 'orbit_leo', 'orbit_meo', 'orbit_po', 'orbit_so', 'orbit_sso',
               'orbit_vleo', 'launchsite_ccsfs_slc_40', 'launchsite_ksc_lc_39a', 'launchsite_vafb_slc_4e']

    training_data = {}
    for key in headers:
        if key == input_data['orbit'] or key == input_data['launchsite']:
            training_data[key] = 1.0
        elif key in list(input_data.keys()):
            training_data[key] = input_data[key]
        else:
            training_data[key] = 0

    values = np.array(list(training_data.values())).astype(float)
    values = values.reshape(1, -1)
    df = pandas.DataFrame(values, columns=list(training_data.keys()))

    prediction = model.predict(df)

    return prediction

