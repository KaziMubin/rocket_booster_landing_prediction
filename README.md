# Rocket_Booster_Landing_Prediction

### Introduction:
This project was a hands-on lab for the "IBM Data Science Capstone Project," which I followed and expanded to practice MLOps
and the deployment of a data science project. I gathered information from the SpaceX API and processed it for Falcon 9
booster landing predictions, determining whether the booster would land successfully. After collecting the data, I cleaned
and processed it to prepare it for the project. Then, I utilized the Plotly Dash framework to create and display a simple 
dashboard for the data. Also used a simple classifier to get a prediction.

### File structure:
- **Data** folder is where all the dataset and processed collected data are kept.
- In **Data_collection and proccessing** I collected data from live APIs and also with web-scraping. This is the data pipeline of MLOps.
- **Datavisualization** folder holds all the `.ipynb` and `.py` files for data visualization.
- **inference_pipeline** folder is for rest api access and automatically getting prediction from trained models.
- **ML_model** is the training pipeline of MLOps. This part trains model on data and save the trained model in the model registry.
- **Saved_model** keeps all the trained models.

### Project goals:
- Understand Data visualization and dashboard creation.
- Implementation of basic ML model
- Understanding feature store
- Enable API access
- containerize for easy test deployment.
- Realize MLOps with feature, training and inference pipeline
- And deploy to VM

### Data visualization and dashboard creation:
In this application I used plotly-dash for data visualization and creation of dashboard. At first I looked at various features or variables and tried to understand the trend. I did one `seaborn.catplot` for payloadmass and flightnumber. I can see that with increment in flight-number success rate increases. Also for higher payloadmass success rate is better. Later I tried `seaborn.scatterplot` for **Launchsite** versus **Payloadmass**.
```
sns.scatterplot(x='PayloadMass', y='LaunchSite', hue='Class', data=df)
plt.show()
```
I also tried `folium` for plotting on map and have a visualization on maps perspective. 
```
site = [[28.562302,	-80.577356], [28.563197, -80.576820], [28.573255, -80.646895], [34.632834, -120.610745]]
map = folium.Map([31.679925498773336, -99.01402453875897], zoom_start=5)
position = folium.map.FeatureGroup()

for pos in site:
    print(pos)
    folium.Marker(pos, popup=pos).add_to(map)
    
map.add_child(position)
```
I put marked the Launchsites on the map and put marker for every launch from that site. Also color of the marker indicates the launch outcome.
```
site_map.add_child(marker_cluster)
for index, record in spacex_df.iterrows():
    marker = folium.Marker(
        location=[record['Lat'], record['Long']],
        icon=folium.Icon(
            color= 'white',
            icon= 'fa-info-circle',
            icon_color= record['Marker_color'],
            prefix='fa'
        ),
        popup=record['class'],
    )
    marker_cluster.add_child(marker)

```
As all this plots are plotted in jupyter notebook and are not interactive. So later I used plotly-dash for creating a interactive visualization dashboard. I just used two types of plot a pi-chart and a scatter plot for each launchsite and payload. Interactive part was enabled by plotly `callback()` functions.
```
@app.callback(Output(component_id='success-pie-chart', component_property='figure'),
              Output(component_id='success-payload-scatter-chart', component_property='figure'),
              Input(component_id='site-dropdown', component_property='value'),
              Input(component_id='payload-slider', component_property='value'))
def get_pie_chart(entered_site, payload_mass):
    filtered_df = spacex_df.copy()
    double_filtered_df = filtered_df[(filtered_df['Payload Mass (kg)'] >= payload_mass[0]) &
                                     (filtered_df['Payload Mass (kg)'] <= payload_mass[1])]
    print(payload_mass)
    if entered_site == 'All':
        pie_fig = px.pie(filtered_df, values='class', names='Launch Site', title='All Site')
        scatter_fig = px.scatter(double_filtered_df, x=double_filtered_df['Payload Mass (kg)'],
                                 y=double_filtered_df['class'], color=double_filtered_df['Booster Version Category'])
        return pie_fig, scatter_fig
    else:
        current_site = double_filtered_df[double_filtered_df['Launch Site'] == entered_site]
        print(current_site)
        pie_fig = px.pie(current_site, names='class', title=entered_site)
        scatter_fig = px.scatter(current_site, x=current_site['Payload Mass (kg)'], y=current_site['class'],
                                 color=current_site['Booster Version Category'])
        return pie_fig, scatter_fig

```

****Though I could add much more informative charts, But for now at first I want to deploy ML model and then focus more on data visualization.*

### ML model
I trained the dataset on multiple Machine learning models. I used `logisticregression`, `svc`, `decissiontreeclassifier` and ``KNeighborsClassifier`. To tune the models for best hyper-parameters, I took advantage of `gridsearchCV` from `sklearn`.
```
parameters ={"C":[0.01,0.1,1],
            'penalty':['l2'], 
            'solver':['lbfgs']}# l1 lasso l2 ridge
lr=LogisticRegression()
logreg_cv = GridSearchCV(lr, param_grid=parameters, cv=10)
logreg_cv.fit(X_train, np.ravel(y_train))
```
All the models are dumped in pickle file and stored in the model registry from `hopsworks.ai`.
```
mr = project.get_model_registry()

model_lr = mr.python.create_model(
    name= "lrmodel",
    description= "logistich reggression model",
    version= 1,
    input_example=X_train.sample(),
    model_schema= model_schema,
)
model_lr.save(lr_pckl)
```
It gave the best hyper-parameter set for which the model performs the best. After calculating the best performíng model, for deployment I will use that specific model.

### feature store
I am adding Hopsworks.ai feature store for better data management. FeatureGroup is being created after collecting and processing the data. After proccesiing the collected data only the feature data are stored in `featuregroup`.
```
project = hopsworks.login()
fs = project.get_feature_store()

#fs.delete_featuregroup('rocket_launch')
rocket_launch_fg = fs.get_or_create_feature_group(
    name="rocket_launch",
    version=1,
    description="Booster landing success prediction",
    primary_key=['FlightNumber']
)
```
And a `featureview` is added for collecting data from ``featuregroup` to train the models.
```
project = hw.login()
fs = project.get_feature_store()

feature_view = fs.get_or_create_feature_view(
    name= "rocket_launch_view",
    description= "feature view fro booster landing success prediction.",
    query= training_set,
    labels= ["predictions"],
    transformation_functions= transformation_function,
    version= 1
)
```
After getting the data from `featureview`, those were used to train the models. And all the models were saved in model registry from `hopsworks.ai`.

### API access
I put the saved model in a RESTful API so It can be deployed and accessed easily. I used django rest-framework to implement the REST-Api.

### Containerization

### Pipeline and deployment



